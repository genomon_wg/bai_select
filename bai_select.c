#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <assert.h>

#ifdef VERBOSE
#define PRINT_MSG(...)  fprintf(stderr, __VA_ARGS__)
#else
#define PRINT_MSG(...)  ((void)0)  
#endif

#define BGZF_TERMINAL_BLOCK_SIZE    28

#define BGZF_HEADER_LEN (12+6)

#define PSEUD_BIN   37450


static int big_endian = 0;

void check_endian(void)
{
    int i = 1;
    if (*((char *)&i + (sizeof(int) - 1))) big_endian = 1;
}


static inline void swap32(void *p)
{
    uint32_t *u = p;
    *u = ((*u << 24) & 0xff000000) | ((*u <<  8) & 0x00ff0000)
       | ((*u >>  8) & 0x0000ff00) | ((*u >> 24) & 0x000000ff);
}

static inline void swap64(void *p)
{
    uint64_t *u = p;
    *u = ((*u << 56) & 0xff00000000000000)
       | ((*u << 40) & 0x00ff000000000000)
       | ((*u << 24) & 0x0000ff0000000000)
       | ((*u <<  8) & 0x000000ff00000000)
       | ((*u >>  8) & 0x00000000ff000000)
       | ((*u >> 24) & 0x0000000000ff0000)
       | ((*u >> 40) & 0x000000000000ff00)
       | ((*u >> 56) & 0x00000000000000ff);
}


static inline uint32_t read_uint32(FILE *fp)
{
    uint32_t v;
    int n_read = fread(&v, 4, 1, fp);
    assert(n_read == 1);
    if (big_endian) swap32(&v);
    return v;
}

static inline void write_uint32(FILE *fp, uint32_t v)
{
    if (big_endian) swap32(&v);
    int n_write = fwrite(&v, 4, 1, fp);
    assert(n_write == 1);
}


static inline int32_t read_int32(FILE *fp)
{
    int32_t v;
    int n_read = fread(&v, 4, 1, fp);
    assert(n_read == 1);
    if (big_endian) swap32(&v);
    return v;
}

static inline void write_int32(FILE *fp, int32_t v)
{
    if (big_endian) swap32(&v);
    int n_write = fwrite(&v, 4, 1, fp);
    assert(n_write == 1);
}


static inline uint64_t read_uint64(FILE *fp)
{
    uint64_t v;
    int n_read = fread(&v, 8, 1, fp);
    assert(n_read == 1);
    if (big_endian) swap64(&v);
    return v;
}

static inline void write_uint64(FILE *fp, uint64_t v)
{
    if (big_endian) swap64(&v);
    int n_write = fwrite(&v, 8, 1, fp);
    assert(n_write == 1);
}


int main(int argc, char *argv[])
{
    if (argc !=  4) {
        fprintf(stderr, "%s ref in_bai out_bai\n", argv[0]);
        exit(1);
    }

    int ref = atoi(argv[1]);

    FILE *fin = fopen(argv[2], "r");
    if (fin == NULL) {
        perror(argv[2]);
        exit(1);
    }

    FILE *fout = fopen(argv[3], "w");
    if (fin == NULL) {
        perror(argv[3]);
        exit(1);
    }

    check_endian();

    char buf[32];

    int n_read = fread(buf, 1, 4, fin);
    assert(n_read == 4);
    assert(strncmp(buf, "BAI\1", 4) == 0);
    int n_write = fwrite(buf, 1, 4, fout);
    assert(n_write == 4);

    int n_ref = read_int32(fin);
    //assert(1 <= ref && ref <= n_ref);
    assert(1 <= ref);
    if (ref > n_ref) {
        fprintf(stderr, "***warn: ref > n_ref\n");
        fprintf(stderr, "         ref=%d, n_ref=%d\n", ref, n_ref);
    }

    write_int32(fout, n_ref);

    for (int i_ref = 1; i_ref <= n_ref; i_ref++) {
        PRINT_MSG("    ref: %d\n", i_ref);
        int n_bin = read_int32(fin);
        PRINT_MSG("        n_bin =  %d\n", n_bin);
        write_int32(fout, i_ref == ref ? n_bin : 0);

        for (int i_bin = 0; i_bin < n_bin; i_bin++) {
            uint32_t bin = read_uint32(fin);    
            int n_chunk = read_int32(fin);
            if (i_ref == ref) write_uint32(fout, bin);
            if (i_ref == ref) write_int32(fout, n_chunk);
            if (bin != PSEUD_BIN) {
                PRINT_MSG("        bin %u\n", bin);
                PRINT_MSG("            n_chunk = %d\n", n_chunk);
                for (int i_chunk = 0; i_chunk < n_chunk; i_chunk++) {
                    n_read = fread(buf, 1, 16, fin);
                    assert(n_read == 16);
                    if (i_ref == ref) {
                        n_write = fwrite(buf, 1, 16, fout);
                        assert(n_write == 16);
                    }
                }
            } else {
                /* pseud-bin */
                PRINT_MSG("        bin %u (pseud-bin)\n", bin);
                assert(n_chunk == 2);
                n_read = fread(buf, 1, 32, fin);
                assert(n_read == 32);
                if (i_ref == ref) {
                    n_write = fwrite(buf, 1, 32, fout);
                    assert(n_write == 32);
                }
            }
        }

        int n_intv = read_int32(fin);
        write_int32(fout, i_ref == ref ? n_intv : 0);
        PRINT_MSG("        n_intv = %d\n", n_intv);
        for (int i_intv = 0; i_intv < n_intv; i_intv++) {
            n_read = fread(buf, 1, 8, fin);
            assert(n_read == 8);
            if (i_ref == ref) {
                n_write = fwrite(buf, 1, 8, fout);
                assert(n_write == 8);
            }
        }
    }
}
