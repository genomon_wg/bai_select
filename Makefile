#CC = gcc
#CFLAGS = -Wall
CC = fccpx
CFLAGS = -Nclang -Ofast -Wall

#CFLAGS += -DVERBOSE

PROGRAM = bai_select

OBJS = bai_select.o

all: $(PROGRAM)

$(PROGRAM): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $@

.c.o:
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f $(OBJS)

distclean: clean
	rm -f $(PROGRAM)
